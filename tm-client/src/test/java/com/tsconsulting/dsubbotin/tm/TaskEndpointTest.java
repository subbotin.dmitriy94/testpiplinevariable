package com.tsconsulting.dsubbotin.tm;

import com.sun.xml.ws.fault.ServerSOAPFaultException;
import com.tsconsulting.dsubbotin.tm.endpoint.*;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.marker.SoapCategory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class TaskEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    @NotNull
    private final TaskEndpoint taskEndpoint;

    @NotNull
    private final ProjectEndpoint projectEndpoint;

    @NotNull
    private final ProjectTaskEndpoint projectTaskEndpoint;

    @NotNull
    private final String taskName = "taskName";

    @Nullable
    private String taskId;

    @NotNull
    private TaskDTO task;

    @Nullable
    private String projectId;

    @NotNull
    private SessionDTO session;

    public TaskEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
        @NotNull final TaskEndpointService taskEndpointService = new TaskEndpointService();
        taskEndpoint = taskEndpointService.getTaskEndpointPort();
        @NotNull final ProjectEndpointService projectEndpointService = new ProjectEndpointService();
        projectEndpoint = projectEndpointService.getProjectEndpointPort();
        @NotNull final ProjectTaskEndpointService projectTaskEndpointService = new ProjectTaskEndpointService();
        projectTaskEndpoint = projectTaskEndpointService.getProjectTaskEndpointPort();
    }

    @Before
    @SneakyThrows
    public void initializeTest() {
        session = sessionEndpoint.openSession("user", "user");
        task = taskEndpoint.createTask(session, taskName, "taskDescription");
        taskId = task.getId();
        projectId = projectEndpoint.createProject(session, "project", "project").getId();
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllTask() {
        Assert.assertEquals(1, taskEndpoint.findAllTask(session, Sort.CREATED.toString()).size());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findTask() {
        Assert.assertNotNull(taskId);
        Assert.assertNotNull(taskEndpoint.findByIdTask(session, taskId));
        Assert.assertNotNull(taskName);
        Assert.assertNotNull(taskEndpoint.findByNameTask(session, taskName));
        Assert.assertNotNull(taskEndpoint.findByIndexTask(session, 1));
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeByIdTask() {
        Assert.assertNotNull(taskId);
        removeTask(taskId);
        boolean found;
        try {
            taskEndpoint.findAllTask(session, null);
            found = true;
        } catch (ServerSOAPFaultException | AbstractException_Exception e) {
            found = !e.getMessage().contains("Task not found!");
        }
        Assert.assertFalse(found);
        taskId = null;
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void createTask() {
        @NotNull final String tempTaskId = taskEndpoint.createTask(session, "task", "task").getId();
        Assert.assertNotNull(tempTaskId);
        Assert.assertNotNull(taskEndpoint.findByIdTask(session, tempTaskId));
        removeTask(tempTaskId);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateByIdTask() {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskName";
        taskEndpoint.updateByIdTask(session, taskId, newTaskName, newTaskDescription);
        @NotNull final TaskDTO updatedTask = taskEndpoint.findByIdTask(session, taskId);
        Assert.assertEquals(task.getId(), updatedTask.getId());
        Assert.assertNotEquals(task.getName(), updatedTask.getName());
        Assert.assertNotEquals(task.getDescription(), updatedTask.getDescription());
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void startByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.startByIdTask(session, taskId);
        Assert.assertEquals(taskEndpoint.findByIdTask(session, taskId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void finishByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.finishByIdTask(session, taskId);
        Assert.assertEquals(taskEndpoint.findByIdTask(session, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void updateStatusByIdTask() {
        Assert.assertEquals(task.getStatus(), Status.NOT_STARTED);
        taskEndpoint.updateStatusByIdTask(session, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskEndpoint.findByIdTask(session, taskId).getStatus(), Status.IN_PROGRESS);
        taskEndpoint.updateStatusByIdTask(session, taskId, Status.COMPLETED);
        Assert.assertEquals(taskEndpoint.findByIdTask(session, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void bindTaskToProject() {
        Assert.assertNull(task.getProjectId());
        projectTaskEndpoint.bindTaskToProject(session, projectId, taskId);
        task = taskEndpoint.findByIdTask(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void unbindTaskFromProject() {
        Assert.assertNull(task.getProjectId());
        projectTaskEndpoint.bindTaskToProject(session, projectId, taskId);
        task = taskEndpoint.findByIdTask(session, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
        projectTaskEndpoint.unbindTaskFromProject(session, projectId, taskId);
        Assert.assertNull(taskEndpoint.findByIdTask(session, taskId).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void findAllTasksByProjectId() {
        Assert.assertNull(task.getProjectId());
        projectTaskEndpoint.bindTaskToProject(session, projectId, taskId);
        @NotNull final String task2Id = taskEndpoint.createTask(session, "task2", "task2").getId();
        projectTaskEndpoint.bindTaskToProject(session, projectId, task2Id);
        Assert.assertEquals(2, projectTaskEndpoint.findAllTasksByProjectId(session, projectId).size());
        removeTask(task2Id);
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeProjectById() {
        Assert.assertNotNull(projectId);
        Assert.assertNotNull(taskId);
        projectTaskEndpoint.bindTaskToProject(session, projectId, taskId);
        removeProject(projectId);
        projectId = null;
        boolean found;
        try {
            projectEndpoint.findAllProject(session, null);
            found = true;
        } catch (ServerSOAPFaultException | AbstractException_Exception e) {
            found = !e.getMessage().contains("Project not found!");
        }
        Assert.assertFalse(found);
        Assert.assertNull(taskEndpoint.findByIdTask(session, taskId).getProjectId());
    }

    @Test
    @SneakyThrows
    @Category(SoapCategory.class)
    public void removeAllProject() {
        @NotNull final String tempTaskId = taskEndpoint.createTask(session, "t2", "t2").getId();
        @NotNull final String project2Id = projectEndpoint.createProject(session, "p2", "p2").getId();
        projectTaskEndpoint.bindTaskToProject(session, projectId, taskId);
        projectTaskEndpoint.bindTaskToProject(session, project2Id, tempTaskId);
        Assert.assertEquals(projectId, taskEndpoint.findByIdTask(session, taskId).getProjectId());
        Assert.assertEquals(project2Id, taskEndpoint.findByIdTask(session, tempTaskId).getProjectId());
        Assert.assertNotNull(projectId);
        removeProject(projectId);
        projectId = null;
        removeProject(project2Id);
        Assert.assertNull(taskEndpoint.findByIdTask(session, taskId).getProjectId());
        Assert.assertNull(taskEndpoint.findByIdTask(session, tempTaskId).getProjectId());
        removeTask(tempTaskId);
    }


    @After
    @SneakyThrows
    public void finalizeTest() {
        if (taskId != null) removeTask(taskId);
        if (projectId != null) removeProject(projectId);
        sessionEndpoint.closeSession(session);
    }

    @SneakyThrows
    private void removeTask(@NotNull final String id) {
        taskEndpoint.removeByIdTask(session, id);
    }

    @SneakyThrows
    private void removeProject(@NotNull final String id) {
        projectTaskEndpoint.removeProjectById(session, id);
    }

}

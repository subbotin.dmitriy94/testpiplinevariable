package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ArgumentsDisplayCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "arguments";
    }

    @Override
    @NotNull
    public String arg() {
        return "-arg";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list arguments.";
    }

    @Override
    public void execute() {
        for (final AbstractSystemCommand command : endpointLocator.getCommandService().getArguments()) {
            @Nullable final String argument = command.arg();
            if (argument != null && !argument.isEmpty()) TerminalUtil.printMessage(argument);
        }
    }

}

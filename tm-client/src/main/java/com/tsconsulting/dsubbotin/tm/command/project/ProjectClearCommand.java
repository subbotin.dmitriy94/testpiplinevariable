package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove all projects.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        endpointLocator.getProjectTaskEndpoint().removeAllProject(session);
        TerminalUtil.printMessage("[Clear]");
    }

}

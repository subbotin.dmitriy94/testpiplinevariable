package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionDTO;
import com.tsconsulting.dsubbotin.tm.endpoint.UserDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class UserIsAuthCommand extends AbstractUserCommand {

    @Override
    @NotNull
    public String name() {
        return "user-auth";
    }

    @Override
    @NotNull
    public String description() {
        return "Display authorized user.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable SessionDTO session = endpointLocator.getSessionService().getSession();
        @NotNull final UserDTO user = endpointLocator.getUserEndpoint().getUser(session);
        showUser(user);
    }

}

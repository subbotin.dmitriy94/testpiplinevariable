package com.tsconsulting.dsubbotin.tm.api.endpoint;

import com.tsconsulting.dsubbotin.tm.dto.SessionDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @NotNull
    @WebMethod
    SessionDTO openSession(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password
    ) throws AbstractException;

    @WebMethod
    boolean closeSession(
            @NotNull @WebParam(name = "session") SessionDTO session
    );

    @NotNull
    @WebMethod
    SessionDTO register(
            @NotNull @WebParam(name = "login") String login,
            @NotNull @WebParam(name = "password") String password,
            @NotNull @WebParam(name = "email") String email
    ) throws AbstractException;

    @NotNull
    @WebMethod
    String getHost();

}

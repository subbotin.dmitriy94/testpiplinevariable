package com.tsconsulting.dsubbotin.tm.repository.entity;

import com.tsconsulting.dsubbotin.tm.api.repository.entity.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.entity.Task;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.TaskNotFoundException;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractOwnerRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task", Task.class).getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll(@NotNull final String userId) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public Task findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM Task WHERE id = :id", Task.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId AND id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM Task", Task.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @NotNull
    @Override
    public Task findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId AND name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getResultStream()
                .findFirst()
                .orElseThrow(TaskNotFoundException::new);
    }

    @Override
    public void clear() {
        findAll().forEach(entityManager::remove);
    }

    @Override
    public void clear(@NotNull final String userId) {
        findAll(userId).forEach(entityManager::remove);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return entityManager
                .createQuery("FROM Task WHERE project.id = :projectId", Task.class)
                .setParameter("projectId", id)
                .getResultList();
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String id) {
        entityManager
                .createQuery("UPDATE Task SET project.id = NULL WHERE user.id = :userId AND project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", id)
                .executeUpdate();
    }

    @Override
    public boolean existById(@NotNull String userId, @NotNull String taskId) {
        return entityManager
                .createQuery("FROM Task WHERE user.id = :userId AND id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", taskId)
                .getResultStream()
                .findFirst()
                .isPresent();
    }

}

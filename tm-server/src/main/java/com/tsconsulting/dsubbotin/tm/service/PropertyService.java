package com.tsconsulting.dsubbotin.tm.service;

import com.jcabi.manifests.Manifests;
import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.InputStream;
import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT_VALUE = "1.42.1";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT_VALUE = "1";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT_VALUE = "a";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SIGNATURE_ITERATION_KEY = "signature.iteration";

    @NotNull
    private static final String SIGNATURE_ITERATION_DEFAULT_VALUE = "1";

    @NotNull
    private static final String SIGNATURE_SECRET_KEY = "signature.secret";

    @NotNull
    private static final String SIGNATURE_SECRET_DEFAULT_VALUE = "a";

    @NotNull
    private static final String DEVELOPER_NAME_DEFAULT_VALUE = "Dima Subbotin";

    @NotNull
    private static final String DEVELOPER_EMAIL_DEFAULT_VALUE = "subbotin.dmitriy94@gmail.com";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT_VALUE = "localhost";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT_VALUE = "8081";

    @NotNull
    private static final String JDBC_DRIVER_KEY = "jdbc.driver";

    @NotNull
    private static final String JDBC_DRIVER_DEFAULT_VALUE = "org.postgresql.Driver";

    @NotNull
    private static final String JDBC_URL_KEY = "jdbc.url";

    @NotNull
    private static final String JDBC_URL_DEFAULT_VALUE = "jdbc:postgresql://localhost/task-manager";

    @NotNull
    private static final String JDBC_LOGIN_KEY = "jdbc.user";

    @NotNull
    private static final String JDBC_LOGIN_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String JDBC_PASSWORD_KEY = "jdbc.password";

    @NotNull
    private static final String JDBC_PASSWORD_DEFAULT_VALUE = "postgres";

    @NotNull
    private static final String HIBERNATE_DIALECT_KEY = "hibernate.dialect";

    @NotNull
    private static final String HIBERNATE_DIALECT_DEFAULT_VALUE = "org.hibernate.dialect.PostgreSQLDialect";

    @NotNull
    private static final String HIBERNATE_SHOW_SQL_KEY = "hibernate.show_sql";

    @NotNull
    private static final String HIBERNATE_SHOW_SQL_DEFAULT_VALUE = "false";

    @NotNull
    private static final String HIBERNATE_FORMAT_SQL_KEY = "hibernate.format_sql";

    @NotNull
    private static final String HIBERNATE_FORMAT_SQL_DEFAULT_VALUE = "false";

    @NotNull
    private static final String HIBERNATE_HBM_2_DDL_KEY = "hibernate.hbm2ddl";

    @NotNull
    private static final String HIBERNATE_HBM_2_DDL_DEFAULT_VALUE = "none";

    @NotNull
    private static final String HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE_KEY = "hibernate.cache.use_second_level_cache";

    @NotNull
    private static final String HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE_DEFAULT_VALUE = "false";

    @NotNull
    private static final String HIBERNATE_CACHE_USE_QUERY_CACHE_KEY = "hibernate.cache.use_query_cache";

    @NotNull
    private static final String HIBERNATE_CACHE_USE_QUERY_CACHE_DEFAULT_VALUE = "false";

    @NotNull
    private static final String HIBERNATE_CACHE_USE_MINIMAL_PUTS_KEY = "hibernate.cache.use_minimal_puts";

    @NotNull
    private static final String HIBERNATE_CACHE_USE_MINIMAL_PUTS_DEFAULT_VALUE = "false";

    @NotNull
    private static final String HIBERNATE_CACHE_HAZELCAST_USE_LITE_MEMBER_KEY = "hibernate.cache.hazelcast.use_lite_member";

    @NotNull
    private static final String HIBERNATE_CACHE_HAZELCAST_USE_LITE_MEMBER_DEFAULT_VALUE = "false";

    @NotNull
    private static final String HIBERNATE_CACHE_REGION_PREFIX_KEY = "hibernate.cache.region_prefix";

    @NotNull
    private static final String HIBERNATE_CACHE_REGION_PREFIX_DEFAULT_VALUE = "task-manager";

    @NotNull
    private static final String HIBERNATE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_KEY = "hibernate.cache.provider_configuration_file_resource_path";

    @NotNull
    private static final String HIBERNATE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_DEFAULT_VALUE = "hazelcast.xml";

    @NotNull
    private static final String HIBERNATE_CACHE_REGION_FACTORY_CLASS_KEY = "hibernate.cache.region.factory_class";

    @NotNull
    private static final String HIBERNATE_CACHE_REGION_FACTORY_CLASS_DEFAULT_VALUE = "com.hazelcast.hibernate.HazelcastLocalCacheRegionFactory";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return getValue("buildVersion", APPLICATION_VERSION_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDeveloperName() {
        return getValue("developer", DEVELOPER_NAME_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getDeveloperEmail() {
        return getValue("email", DEVELOPER_EMAIL_DEFAULT_VALUE);
    }

    @Override
    public int getPasswordIteration() {
        return Integer.parseInt(getPropertyValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT_VALUE));
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getPropertyValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT_VALUE);
    }

    @Override
    public int getSignatureIteration() {
        return Integer.parseInt(getPropertyValue(SIGNATURE_ITERATION_KEY, SIGNATURE_ITERATION_DEFAULT_VALUE));
    }

    @NotNull
    @Override
    public String getSignatureSecret() {
        return getPropertyValue(SIGNATURE_SECRET_KEY, SIGNATURE_SECRET_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getPropertyValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getServerPort() {
        return getPropertyValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcDriver() {
        return getPropertyValue(JDBC_DRIVER_KEY, JDBC_DRIVER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcUrl() {
        return getPropertyValue(JDBC_URL_KEY, JDBC_URL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcLogin() {
        return getPropertyValue(JDBC_LOGIN_KEY, JDBC_LOGIN_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getJdbcPassword() {
        return getPropertyValue(JDBC_PASSWORD_KEY, JDBC_PASSWORD_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateDialect() {
        return getPropertyValue(HIBERNATE_DIALECT_KEY, HIBERNATE_DIALECT_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateShowSql() {
        return getPropertyValue(HIBERNATE_SHOW_SQL_KEY, HIBERNATE_SHOW_SQL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateFormatSql() {
        return getPropertyValue(HIBERNATE_FORMAT_SQL_KEY, HIBERNATE_FORMAT_SQL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateHbm2Ddl() {
        return getPropertyValue(HIBERNATE_HBM_2_DDL_KEY, HIBERNATE_HBM_2_DDL_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheUseSecondLevelCache() {
        return getPropertyValue(HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE_KEY, HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheUseQueryCache() {
        return getPropertyValue(HIBERNATE_CACHE_USE_QUERY_CACHE_KEY, HIBERNATE_CACHE_USE_QUERY_CACHE_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheUseMinimalPuts() {
        return getPropertyValue(HIBERNATE_CACHE_USE_MINIMAL_PUTS_KEY, HIBERNATE_CACHE_USE_MINIMAL_PUTS_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheHazelcastUseLiteMember() {
        return getPropertyValue(HIBERNATE_CACHE_HAZELCAST_USE_LITE_MEMBER_KEY, HIBERNATE_CACHE_HAZELCAST_USE_LITE_MEMBER_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheRegionPrefix() {
        return getPropertyValue(HIBERNATE_CACHE_REGION_PREFIX_KEY, HIBERNATE_CACHE_REGION_PREFIX_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheProviderConfigurationFileResourcePath() {
        return getPropertyValue(HIBERNATE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_KEY, HIBERNATE_CACHE_PROVIDER_CONFIGURATION_FILE_RESOURCE_PATH_DEFAULT_VALUE);
    }

    @NotNull
    @Override
    public String getHibernateCacheRegionFactoryClass() {
        return getPropertyValue(HIBERNATE_CACHE_REGION_FACTORY_CLASS_KEY, HIBERNATE_CACHE_REGION_FACTORY_CLASS_DEFAULT_VALUE);
    }

    @NotNull
    private String getPropertyValue(@NotNull final String property, @NotNull final String defaultValue) {
        if (System.getProperties().contains(property)) return System.getProperty(property);
        if (System.getenv().containsKey(property)) return System.getenv(property);
        return properties.getProperty(property, defaultValue);
    }

    @NotNull
    private String getValue(@NotNull final String entry, @NotNull final String defaultValue) {
        if (Manifests.exists(entry)) return Manifests.read(entry);
        return defaultValue;
    }

}

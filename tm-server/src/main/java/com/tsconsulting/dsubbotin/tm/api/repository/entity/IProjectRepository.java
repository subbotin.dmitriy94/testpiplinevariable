package com.tsconsulting.dsubbotin.tm.api.repository.entity;

import com.tsconsulting.dsubbotin.tm.entity.Project;
import org.jetbrains.annotations.NotNull;

public interface IProjectRepository extends IOwnerRepository<Project> {

    boolean existById(@NotNull String userId, @NotNull String projectId);

}

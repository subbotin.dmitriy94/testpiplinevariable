package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.dto.EntityLogDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.EntityOperationType;
import org.jetbrains.annotations.NotNull;

public interface IActiveMqService {

    @NotNull
    EntityLogDTO createMessage(@NotNull Object entity, @NotNull EntityOperationType operationType);

    void send(@NotNull EntityLogDTO entity);

}

package com.tsconsulting.dsubbotin.tm.service.entity;

import com.tsconsulting.dsubbotin.tm.api.service.IConnectionService;
import com.tsconsulting.dsubbotin.tm.api.service.ILogService;
import com.tsconsulting.dsubbotin.tm.api.service.entity.IOwnerService;
import com.tsconsulting.dsubbotin.tm.entity.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    public AbstractOwnerService(@NotNull IConnectionService connectionService, @NotNull ILogService logService) {
        super(connectionService, logService);
    }

}

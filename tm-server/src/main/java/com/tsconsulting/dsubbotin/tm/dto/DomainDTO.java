package com.tsconsulting.dsubbotin.tm.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@JsonRootName(value = "domain")
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class DomainDTO implements Serializable {

    @NotNull
    private Date date;

    @NotNull
    @XmlElement(name = "user")
    @XmlElementWrapper(name = "users")
    private List<UserDTO> users;

    @NotNull
    @XmlElement(name = "project")
    @XmlElementWrapper(name = "projects")
    private List<ProjectDTO> projects;

    @NotNull
    @XmlElement(name = "task")
    @XmlElementWrapper(name = "tasks")
    private List<TaskDTO> tasks;

}

package com.tsconsulting.dsubbotin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsconsulting.dsubbotin.tm.api.service.IActiveMqService;
import com.tsconsulting.dsubbotin.tm.dto.EntityLogDTO;
import com.tsconsulting.dsubbotin.tm.enumerated.EntityOperationType;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;
import java.util.Date;

public class ActiveMqService implements IActiveMqService {

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(ActiveMQConnection.DEFAULT_BROKER_URL);

    @NotNull
    @SneakyThrows
    public EntityLogDTO createMessage(@NotNull final Object entity, @NotNull final EntityOperationType operationType) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(entity) + "\n";
        @NotNull final String className = entity.getClass().getSimpleName();
        @NotNull final EntityLogDTO message = new EntityLogDTO(className, new Date(), json, operationType);
        return message;
    }

    @SneakyThrows
    public void send(@NotNull final EntityLogDTO entity) {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Destination destination = session.createTopic("JCG_TOPIC");
        final MessageProducer producer = session.createProducer(destination);
        final ObjectMessage message = session.createObjectMessage(entity);
        producer.send(message);
        producer.close();
        session.close();
        connection.close();
    }

}

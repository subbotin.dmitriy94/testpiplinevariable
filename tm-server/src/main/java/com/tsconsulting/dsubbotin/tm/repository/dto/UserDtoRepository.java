package com.tsconsulting.dsubbotin.tm.repository.dto;

import com.tsconsulting.dsubbotin.tm.api.repository.dto.IUserDtoRepository;
import com.tsconsulting.dsubbotin.tm.dto.UserDTO;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDtoRepository extends AbstractDtoRepository<UserDTO> implements IUserDtoRepository {

    public UserDtoRepository(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @NotNull
    @Override
    public UserDTO findById(@NotNull final String id) throws AbstractException {
        return entityManager
                .createQuery("FROM UserDTO WHERE id = :id", UserDTO.class)
                .setParameter("id", id)
                .getResultStream()
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public UserDTO findByIndex(final int index) throws AbstractException {
        return entityManager
                .createQuery("FROM UserDTO", UserDTO.class)
                .setMaxResults(1)
                .setFirstResult(index)
                .getResultStream()
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @NotNull
    @Override
    public UserDTO findByLogin(@NotNull final String login) throws AbstractException {
        return entityManager
                .createQuery("FROM UserDTO WHERE login = :login", UserDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .setParameter("login", login)
                .getResultStream()
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void clear(@NotNull final String userId) {
        entityManager
                .createQuery("DELETE FROM UserDTO where userId <> :userId")
                .setParameter("userId", userId)
                .executeUpdate();
    }

}

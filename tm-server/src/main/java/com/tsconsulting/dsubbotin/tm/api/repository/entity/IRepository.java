package com.tsconsulting.dsubbotin.tm.api.repository.entity;

import com.tsconsulting.dsubbotin.tm.entity.AbstractEntity;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    @NotNull
    List<E> findAll();

    @NotNull
    E findById(@NotNull String id) throws AbstractException;

    @NotNull
    E findByIndex(int index) throws AbstractException;

    void create(@NotNull E entity);

    void remove(@NotNull E entity);

    void update(@NotNull E entity);

    void clear();

}
package com.tsconsulting.dsubbotin.tm.listener;

import com.tsconsulting.dsubbotin.tm.api.service.ILoggerService;
import com.tsconsulting.dsubbotin.tm.dto.EntityLogDTO;
import com.tsconsulting.dsubbotin.tm.service.LoggerService;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.io.Serializable;

public class LogMessageListener implements MessageListener {

    @NotNull
    final ILoggerService loggerService = new LoggerService();

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof ObjectMessage)) return;
        @NotNull final Serializable entity = ((ObjectMessage) message).getObject();
        if (entity instanceof EntityLogDTO) loggerService.writeLog((EntityLogDTO) entity);
    }

}

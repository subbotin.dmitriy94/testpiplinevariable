package com.tsconsulting.dsubbotin.tm.dto;

import com.tsconsulting.dsubbotin.tm.enumerated.EntityOperationType;
import jakarta.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.Date;

@Getter
@XmlRootElement
@RequiredArgsConstructor
public class EntityLogDTO implements Serializable {

    @NotNull
    private final String className;

    @NotNull
    private final Date date;

    @NotNull
    private final String entity;

    @NotNull
    private final EntityOperationType operationType;

}

package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.dto.EntityLogDTO;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public interface ILoggerService {

    void writeLog(@NotNull EntityLogDTO message) throws IOException;

}
